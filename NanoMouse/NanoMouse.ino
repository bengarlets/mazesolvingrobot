#include <Servo.h>

Servo leftServo;
Servo rightServo;

const byte ledPin = 13;
const byte buttonPin = 9;

const byte power = 200;

void forward()
{
  leftServo.writeMicroseconds(1500+power);
  rightServo.writeMicroseconds(1500-power); 
  
}

void setup() {
  // put your setup code here, to run once:
  leftServo.attach(6);
  rightServo.attach(5);

  pinMode(ledPin,OUTPUT);
  pinMode(buttonPin,INPUT_PULLUP);

  while(digitalRead(buttonPin))
  {}

  forward();

}

void loop() {
  // put your main code here, to run repeatedly:

}